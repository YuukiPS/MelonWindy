#include <cstdio>
#include <cstring>
#include <cstdint>
#include <vector>

#include "lua/lua.hpp"

#ifndef LIBRARY_H
#define LIBRARY_H

extern "C" int compile(const char* script, uint8_t * output);

#endif // LIBRARY_H