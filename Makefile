CC = g++
CFLAGS = -I. -std=c++11
LDFLAGS = -L. -llua

# Specify the object files for your project
OBJECTS = library.o

# Build rule for the main program
main: $(OBJECTS)
	$(CC) $(CFLAGS) -o main $(OBJECTS) $(LDFLAGS)

# Build rule for the library object file
library.o: library.cpp
	$(CC) $(CFLAGS) -c library.cpp

# Clean rule to remove generated files
clean:
	rm -f main $(OBJECTS)